/**
 * The example configuration file
 *
 * Copy this file to config.js to be included in webpack's
 * configuration file.
 */

module.exports = {
	theme: {
		title: 'WP Sandbox',
		slug: 'wpsandbox'
	},
	scripts: {
		main: './src/assets/scripts/main.js',
		fa: './src/assets/scripts/fa.js'
	},
	server: {
		root: '/Users/tj/Projects/WPThemeLabs/wp-themes-dev/webroot',
		path: '/Users/tj/Projects/WPThemeLabs/wp-themes-dev/webroot/wp-content/themes/wpsandbox',
		port: 5678
	}
}
