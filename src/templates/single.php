<?php get_header();?>
	<main class="container max-in">
		<div class="row">
			<div class="col px-md-5 py-5">
			<?php
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();
			?>
				<div class="post-head">
					<?php the_title( '<h1>', '</h1>'); ?>
					<span class="post-meta">
						Published on <?php the_time('F, d m Y'); ?> in Category <?php the_category(); ?>
					</span>
				</div>
				<div class="post-content">
					<?php
						echo apply_filters( 'get_featured_image', get_the_ID() );
					?>
					<?php the_content(); ?>
				</div>
			<?php
					}
				}
			?>
			</div>
		</div>
	</main>
<?php get_footer(); ?>
